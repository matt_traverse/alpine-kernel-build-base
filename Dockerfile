FROM alpine:3.7
RUN apk update && apk add xz wget git make coreutils gcc bash musl-dev bc perl bison flex openssl-dev
RUN wget https://downloads.lede-project.org/snapshots/targets/armvirt/64/openwrt-sdk-armvirt-64_gcc-7.3.0_musl.Linux-x86_64.tar.xz && \
	mkdir -p /opt && \
	tar -C /opt/ -Jxvf openwrt-sdk-armvirt-64_gcc-7.3.0_musl.Linux-x86_64.tar.xz && \
	rm openwrt-sdk-armvirt-64_gcc-7.3.0_musl.Linux-x86_64.tar.xz && \
	rm -rf /opt/openwrt-sdk-armvirt-64_gcc-7.3.0_musl.Linux-x86_64/build_dir/ && \
	rm -rf /opt/openwrt-sdk-armvirt-64_gcc-7.3.0_musl.Linux-x86_64/target

